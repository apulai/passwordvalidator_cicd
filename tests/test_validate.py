import unittest

# A fő modulban (passvalidator) validate a funkciónk neve
from passvalidator import validate

class ValidateTests(unittest.TestCase):

    def test_empty(self):
        self.assertFalse(validate(''))

    def test_too_short(self):
        self.assertFalse(validate('aAbB1!?'))

    def test_too_long(self):
        self.assertFalse(validate('aA1!?' * 4 + 'a'))

    def test_no_number(self):
        self.assertFalse(validate("aAbBcC!?"))

    def test_no_upper(self):
        self.assertFalse(validate("aabbzz()"))

    def test_no_lower(self):
        self.assertFalse(validate("%&AABBCC"))

    def test_no_special(self):
        self.assertFalse(validate("aAbmMC19"))

    def test_valid(self):
        self.assertTrue(validate("a1?mzuF4"))
