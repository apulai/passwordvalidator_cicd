def validate(password: str) -> bool:
    """
    Jelszo validalo

    Egy jelszo akkor valid, ha a kovetkezo kovetelmenyeknek megfelel:
    - 8 es 20 karakter kozotti a hossza (inkluziv)
    - Legalabb egy szamot tartalmaz
    - Legalabb egy kisbetut tartalmaz
    - Legalabb egy nagybetut tartalmaz
    - Legalabb egyet tartalmaz a kovetkezo a karakterekbol: !"§$%&/()=?

    Parameterek:
        password (str): A validalando jelszo

    Returns:
        bool: True ha a jelszo valid, egyebkent False 
    """

    len_ok = (len(password) > 7) and (len(password) <21)
    number_ok = any( i.isdigit() for i in password )
    lowercase_ok = any(i.islower() for i in password)
    uppercase_ok = any(i.isupper() for i in password)
    special_ok = any(i in '!"§$%&/()=?' for i in password)

    return len_ok and number_ok and lowercase_ok and uppercase_ok and special_ok


if __name__ == "__main__":
    print(validate.__doc__)
    input_pass = input('Adj meg egy jelszot!: ')
    print( ('A jelszo nem jo', 'OK') [validate(input_pass)] )


